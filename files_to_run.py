'''This file is used to store the data file names in an array,
so that it can be iterated through and accessed by the rest of the project easily.'''

# Storing directory nav in a variable to make the file entries to the array readable
RESOURCES = 'Resources/'
DHAHRAN = "GeoJson co-ordinates Dhahran-20201217T142542Z-001/GeoJson co-ordinates Dhahran/"
RIYADH = "GeoJson co-ordinates Riyadh-20201217T142613Z-001/GeoJson co-ordinates Riyadh/"
JAN_2021_PATH = "January_2021_files/"

FILES = [
    RESOURCES + DHAHRAN + "Dhahran_aerobunker.geojson",
    RESOURCES + DHAHRAN + "Hills_School.geojson",
    RESOURCES + DHAHRAN + "Khobar_Corniche_Seawalk.geojson",
    RESOURCES + DHAHRAN + "Mall_of_Dhahran.geojson",
    RESOURCES + DHAHRAN + "Prince_Faisal_bin_Fahad_walking_park.geojson",
    RESOURCES + RIYADH + "export-1.geojson",
    RESOURCES + RIYADH + "export-2.geojson",
    RESOURCES + RIYADH + "export-8.geojson",
    RESOURCES + RIYADH + "export-9.geojson",
    RESOURCES + RIYADH + "export-10.geojson",
    RESOURCES + RIYADH + "export-11.geojson",
    RESOURCES + RIYADH + "King_Abdulaziz_historical_park.geojson",
    RESOURCES + RIYADH + "Qasral_Hokm_Iconic_Station.geojson",
    RESOURCES + JAN_2021_PATH + "Buildings - Dhahran.geojson",
    RESOURCES + JAN_2021_PATH + "Highways - Dhahran.geojson",
    RESOURCES + JAN_2021_PATH + "Hills School.geojson",
    RESOURCES + JAN_2021_PATH + "Parking - Dhahran.geojson",
    RESOURCES + JAN_2021_PATH + "Parks - Dhahran.geojson",
    RESOURCES + JAN_2021_PATH + "Schools - Dhahran.geojson"]

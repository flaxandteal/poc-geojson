''' manipulate the data by using the processor generated file to
    create new field names, improving data consistency'''

import json
import files_to_run


def description_key_values(key, number, des, props, description):
    '''Finding and adding the necessary fields from features to the new
        field called 'description'. Creating a string list of key/values'''
    if key in des[number]:
        try:
            description = description + key + \
                ' : ' + str(props[key]) + ", "
        except KeyError as exception:
            print("Description error: ", exception, "\n")
    return description


def all_names_key_values(key, number, nmes, props, all_names):
    '''Finding and adding the necessary fields from features to the new
        field called 'all_names'. Creating a string list of key/values'''
    if key in nmes[number]:
        try:
            all_names = all_names + key + \
                ' : ' + str(props[key]) + ", "
        except KeyError as exception:
            print("List of all name fields error: ", exception, "\n")
    return all_names


def source_key_values(key, number, src, props, source):
    '''Finding and adding the necessary fields from features to the new
        field called 'source'. Creating a string list of key/values'''
    if key in src[number]:
        try:
            source = source + key + ' : ' + \
                str(props[key]) + ", "
        except KeyError as exception:
            print("Source fields error: ", exception, "\n")
    return source


def details_key_values(key, number, dets, props, details):
    '''Finding and adding the necessary fields from features to the new
        field called 'details'. Creating a string list of key/values'''
    if key in dets[number]:
        try:
            details = details + key + \
                ' : ' + str(props[key]) + ", "
        except KeyError as exception:
            print("Detail fields error: ", exception, "\n")
    return details


def read_files(file_name):
    '''Reading the file and storing the json in items.'''
    with open(file_name, 'r') as file_read:
        items = json.load(file_read)
    return items


def open_rule_file(file_output_number, path):
    ''' Open a new file to find the fields that are needed for the new geojson
        store it as fields'''
    if path == "":
        path = 'generated_rules/output'
    with open(path + str(file_output_number)
              + '.json', 'r') as json_file_read:
        fields = json.load(json_file_read)
    return fields


def set_number_of_features(items):
    '''Setting the number of features.'''
    return len(items["features"])


def set_feature(items):
    '''Setting the feature that is taken from the list of items.'''
    return items['features']


def get_files():
    '''Getting the original files that will be processed and manipulated.'''
    return files_to_run.FILES


def manipulate_data():
    ''' Method to manipulate data by creating new fields
        using existing data.'''
    file_output_number = 1
    files = get_files()

    for file in files:
        file_name = str(file)
        items = read_files(file_name)
        number_of_features = set_number_of_features(items)
        feature = set_feature(items)
        print("File number is: ", file_output_number,
              " with ", number_of_features, " features.")

        fields = open_rule_file(file_output_number, '')

    # '''  Iterate through the features ( based on the number of features calc above)
    #     then extract the properties that you need. You can also change and ammend
    #     the data at this stage - i.e the added property of 'name' and 'description' which helps
    #     ensure the consistency of keys in the dataset.'''
        for number in range(number_of_features):
            # for each feature
            if 'properties' in feature[number]:
                # Name is equal to the ID for now
                name = fields["Name"][number]
                description = ""
                all_names = ""
                source = ""
                details = ""
                props = feature[number]['properties']
                for key in props:
                    # Extracting fields for description - describing the
                    # feature
                    des = fields["Description"]
                    description = description_key_values(
                        key, number, des, props, description)

                    # Extracting all fields linked to 'name' such as names in
                    # different languages
                    nmes = fields["All_Names"]
                    all_names = all_names_key_values(
                        key, number, nmes, props, all_names)

                    # Extracing all fields related to the source, such as
                    # source, wikidata/wikipedia
                    src = fields["Source"]
                    source = source_key_values(key, number, src, props, source)

                    # Extracting all the fields related to details  such as
                    # timestamp, username, version
                    dets = fields["Details"]
                    details = details_key_values(
                        key, number, dets, props, details)

                #  adding the new fields to improve the data structure
                props["name"] = name
                props["description"] = description
                props["all_names"] = all_names
                props["source"] = source
                props["details"] = details

        with open('generated/GeoJ_' + str(file_output_number) + '.json', 'w',
                  encoding='utf-8') as f_n:
            json.dump(items, f_n, ensure_ascii=False, indent=4)

        file_output_number = file_output_number + 1


manipulate_data()


'''Testing the data engineer file methods and functionality.'''
import data_engineer_6 as de
import files_to_run

list_of_files = ['tests/test_generated_files/test_output1.json',
                 'tests/test_generated_files/test_output2.json']

def test_file_read():
    '''Checking the file can be read and that it is still a dictonary structure.'''
    items = de.read_files(list_of_files[1])
    assert items != ""
    assert isinstance(items) is dict


def test_open_rule_file():
    '''Checking the file can be read and that it is still a dictonary structure.
       Using the test path directory.'''
    file_output_number = 1
    path = "tests/test_generated_files/test_output"
    fields = de.open_rule_file(file_output_number, path)
    assert isinstance(fields) is dict


def test_get_files():
    '''Testing the retrieval of files.'''
    num = len(list_of_files)
    expected = "tests/test_generated_files/test_output1.json"

    files = de.get_files()
    assert files == files_to_run.FILES
    assert num == 2
    assert expected == list_of_files[0]

def test_manipulate_data(capsys):
    '''Test to ensure that the data engineer runs through successfully.'''
    # Needs processor to run so that the file exists in the dir
    de.manipulate_data()
    expected_output = 'File number is:'
    out, err = capsys.readouterr()
    print(err)
    assert expected_output in out

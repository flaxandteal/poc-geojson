'''File to test the processing of geojson files and generation of new documents.'''
import json
import geojson_processor.processor as processor


#   Mock vars
MOCK_FEATURE_INFO = {"Description": [],
                     "Name": [],
                     "All_Names": [],
                     "Source": [],
                     "Details": []
                     }

mock_files = ['tests/test_mock_files/Mock_Hills School.geojson',
              'tests/test_mock_files/Mock_Prince_Faisal_bin_Fahad_walking_park.geojson']

mock_filenames = ['filename1',
                  'filename2',
                  'mock_file_3',
                  'mock_file_4',
                  'mock_file_5']

def mock_set_data_in_feature_info():
    ''' Setting a mock method to clear variables.'''
    MOCK_FEATURE_INFO["Description"] = ['test', 'desc here']
    MOCK_FEATURE_INFO["All_Names"] = ['test', 'all names here']
    MOCK_FEATURE_INFO["Name"] = ['test', 'name is here']
    MOCK_FEATURE_INFO["Source"] = ['test', 'source here']
    MOCK_FEATURE_INFO["Details"] = ['test', 'details here']


def test_check_mock_files_array():
    ''' Testing the files list.'''
    result = 5
    mock_name = "mock_file_5"

    count = 0
    filename = ""
    for file in mock_filenames:
        filename = file
        assert filename == mock_filenames[count]
        count = count + 1

    assert result == count
    assert mock_name == mock_filenames[result-1]


def test_feature_contents_index_set():
    '''Resetting global vars'''
    mock_set_data_in_feature_info()

    assert MOCK_FEATURE_INFO['Description'][1] == 'desc here'
    assert MOCK_FEATURE_INFO['All_Names'][0] == 'test'
    assert MOCK_FEATURE_INFO['Name'][0] == 'test'
    assert MOCK_FEATURE_INFO['Source'][1] == 'source here'
    assert MOCK_FEATURE_INFO['Details'][0] == 'test'


def test_mock_feature_info_is_set():
    '''Testing the global vars are set correctly.'''
    mock_set_data_in_feature_info()
    expected_result = {"Description":  ['test', 'desc here'],
                       "All_Names": ['test', 'all names here'],
                       "Name":  ['test', 'name is here'],
                       "Source": ['test', 'source here'],
                       "Details": ['test', 'details here']
                       }
    assert MOCK_FEATURE_INFO == expected_result


def test_reset():
    '''Testing the data has successfully been cleared.'''
    expected_empty_data = {"Description": [],
                           "All_Names": [],
                           "Name": [],
                           "Source": [],
                           "Details": []
                           }
    mock_set_data_in_feature_info()
    processor.FEATURE_INFO = MOCK_FEATURE_INFO
    print('mock feature', MOCK_FEATURE_INFO)
    processor.reset_global_feature_vars()
    assert processor.FEATURE_INFO == expected_empty_data


def test_set_and_reset():
    '''Testing the vars at different stages to ensure it is as expected.'''
    expected_empty_data = {"Description": [],
                           "All_Names": [],
                           "Name": [],
                           "Source": [],
                           "Details": []
                           }
    expected_result_added_data = {"Description":  ['test', 'desc here'],
                                  "All_Names": ['test', 'all names here'],
                                  "Name":  ['test', 'name is here'],
                                  "Source": ['test', 'source here'],
                                  "Details": ['test', 'details here']
                                  }
    mock_set_data_in_feature_info()
    assert MOCK_FEATURE_INFO == expected_result_added_data
    assert MOCK_FEATURE_INFO == processor.FEATURE_INFO

    processor.reset_global_feature_vars()
    assert processor.FEATURE_INFO == expected_empty_data


def test_check_run_through(capsys):
    '''Testing a complete run through.'''
    number_of_files = len(mock_files)
    expected_output = 'This is file number: 1\nThis is file number: 2\nYour iteration has complete!'
    processor.get_file_from_list(mock_files)
    out, err = capsys.readouterr()
    print(err)
    assert number_of_files == 2
    assert expected_output in out


def test_json_read_mock_files():
    '''Testing the json read of mocked files.'''
    # file num 1
    file_name = mock_files[1]
    file_input = {}
    result = processor.read_json_file(file_name, file_input)
    assert result['type'] == 'FeatureCollection'
    assert result['type'] != 'word'
    assert result['features'][0]['type'] == 'Feature'
    assert result['features'][0]['properties']['@id'] == 'relation/3667294'
    assert result['features'][0]['properties']['name:en'] == 'Eastern Region'

    # file num 0
    file_name2 = mock_files[0]
    file_input2 = {}
    result = processor.read_json_file(file_name2, file_input2)
    assert result['type'] == 'FeatureCollection'
    assert result['type'] != 'word'
    assert result['features'][0]['type'] == 'Feature'
    assert result['features'][0]['properties']['@id'] == 'relation/1504891'
    assert result['features'][0]['properties']['sport'] == 'athletics'
    assert result['features'][0]['properties']['@id'] != 'r'
    assert result['features'][0]['properties']['sport'] != 'a'


def test_data_extraction_methods_match_new_file():
    '''Testing the field extraction is as expected and matching the
        newly generated file.'''
    names = processor.NAMES
    terms = processor.TERMS
    source = processor.SOURCE
    details = processor.DETAILS
    num = 1
    for i in mock_files:
        result = processor.reset_global_feature_vars()
        new_file_path = 'tests/test_generated_files/test_output' + \
            str(num) + '.json'
        file_name = i
        file_input = {}
        file_input = processor.read_json_file(file_name, file_input)
        result = processor.extracting_fields_from_features(file_input,
                                                           names,
                                                           terms,
                                                           source,
                                                           details)

        with open('tests/test_generated_files/test_output'
                  + str(num) + '.json', 'w', encoding='utf-8') as field_array_gen:
            json.dump(result, field_array_gen, ensure_ascii=False, indent=4)

        num = num+1
        new_file = processor.read_json_file(new_file_path, {})
        assert result == new_file

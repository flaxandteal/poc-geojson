''' Processor to interpret the geojson files .
To extract the fields and place them under an appropiate name.
Such as Description, Name and Source.  '''

import json
import constants_arrays
import files_to_run

# imports potentially needed for processor to work with lintol doorstep
# from dask.threaded import get
# # from ltldoorstep.processor import DoorstepProcessor
# from ltldoorstep.reports.report import combine_reports
# import sys

FEATURE_INFO = {"Description": [],
                "Name": [],
                "All_Names": [],
                "Source": [],
                "Details": []
                }
# Constant arrays
DETAILS = constants_arrays.CONST_DETAILS
NAMES = constants_arrays.CONST_ALL_NAMES_ALL_LANG
SOURCE = constants_arrays.CONST_SOURCE
TERMS = constants_arrays.CONST_DESC
FILES = files_to_run.FILES


def get_files():
    '''Getting the files.'''
    files = []
    files = FILES
    return files


def get_file_from_list(files):
    ''' Get the array of files and iterate through each file.
    Invoke the generate_json_file method so it takes one file
    at a time.'''
    file_input = {}
    number = 1
    for i in files:
        file_name = i
        file_input = read_json_file(file_name, file_input)
        generate_json_file(file_input, number)
        number = number + 1
    print("Your iteration has complete! Check the" +
          " folder in your directory for the newly generated files.")


def read_json_file(file_name, file_input):
    '''Reading the files and returning the content in json format.'''
    with open(file_name) as f_n:
        file_input = json.load(f_n)
    return file_input


def reset_global_feature_vars():
    ''''Reset the feature information variabes so that each file conains
    only what is related to the file.'''
    FEATURE_INFO["Description"] = []
    FEATURE_INFO["All_Names"] = []
    FEATURE_INFO["Name"] = []
    FEATURE_INFO["Source"] = []
    FEATURE_INFO["Details"] = []


def generate_json_file(file_input, number):
    '''Generate json output file to store the relevant field names
    under the array grouping.'''
    # global number
    reset_global_feature_vars()
    print("This is file number:", number)
    extracting_fields_from_features(file_input,
                                    NAMES,
                                    TERMS,
                                    SOURCE,
                                    DETAILS)
    with open('generated_rules/output'
              + str(number) + '.json', 'w', encoding='utf-8') as field_array_gen:
        json.dump(FEATURE_INFO, field_array_gen, ensure_ascii=False, indent=4)


def extracting_fields_from_features(file_input,
                                    names,
                                    terms,
                                    source,
                                    details):
    ''' Method will be called to extract everything from each feature from
    every geojsn file passed through. '''
    for feature in file_input["features"]:
        feature_id = feature["properties"]['@id']
        extract_all_name_fields(feature, names)
        extract_description_field(feature, terms)
        extract_details_field(feature, details)
        extract_name_field(feature_id)
        extract_source_field(feature, source)
    return FEATURE_INFO


#  Extracting the terms that match with the feature fields and
#  storing it in the necessary array grouping.
#  Terms are stored in the constants_strings.py file and
#  new terms can be added as they are discovered. and then added
#  to contants_arrays.py groupings as appropiate.


def extract_name_field(feature_id):
    ''' Extract the 'name' field.
        This is ID for the moment as this is consistently in datasets'''

    FEATURE_INFO["Name"].append(feature_id)


def extract_description_field(feature, terms):
    ''' extract the description fields '''

    description_for_feature = []
    FEATURE_INFO["Description"].append(description_for_feature)
    for item in feature["properties"]:
        for word in terms:
            if item == word:
                description_for_feature.append(item)


def extract_details_field(feature, details):
    ''' extract detail fields '''
    details_for_feature = []
    FEATURE_INFO["Details"].append(details_for_feature)
    for item in feature["properties"]:
        for word in details:
            if item == word:
                details_for_feature.append(item)


def extract_source_field(feature, source):
    ''' extract source field'''
    source_for_feature = []
    FEATURE_INFO["Source"].append(source_for_feature)
    for item in feature["properties"]:
        for word in source:
            if item == word:
                source_for_feature.append(item)


def extract_all_name_fields(feature, names):
    ''' extract all fields associated with naming '''
    names_for_feature = []
    FEATURE_INFO["All_Names"].append(names_for_feature)
    for item in feature["properties"]:
        for word in names:
            if item == word:
                names_for_feature.append(item)


# ''' commented out code - unsure if format is essential or not'''
    # def check_ids_surjective(json, rprt):
    #     # IDs are surjective onto their range
    #     if 'ID' in json:
    #         ids = json['ID']
    #         unique_ids = len(set(ids))
    #         expected_ids = max(ids) - min(ids) + 1
    #         if expected_ids != unique_ids:
    #             rprt.add_issue(
    #                 logging.WARNING,
    #                 'check-ids-surjective:not-surjective',
    #                 ("IDs are missing, %d missing between %d and %d") % \
    #                 (expected_ids - unique_ids, min(ids), max(ids)),
    #                 error_data={
    #                     'missing-count': expected_ids - unique_ids,
    #                     'lowest-id': min(ids),
    #                     'highest-id': max(ids)
    #                 }
    #             )
    # def check_ids_unique(json, rprt):
    #     # IDs are unique
    #     if 'ID' in json:
    #         ids = json['ID']
    #         min_duplicates = len(set(ids)) < len(ids)
    #         if min_duplicates > 0:
    #             rprt.add_issue(
    #                 'lintol/json-checker:1',
    #                 logging.WARNING,
    #                 'check-ids-unique:ids-not-unique',
    #                 ("IDs are not unique, at least %d duplicates") % min_duplicates,
    #             )
    #     return rprt
# class geojsonCheckerProcessor(DoorstepProcessor):

class GeojsonCheckerProcessor():
    ''' Running the processor for geojson files '''
    code = 'lintol-geojson-custom-example:1'
    description = ("geojson Checker Processor")
    preset = 'tabular'
    files = get_files()
    get_file_from_list(files)

processor = GeojsonCheckerProcessor

if __name__ == "__main__":
    #  unsure of the purpose for argv ?
    # argv = sys.argv
    # processor = geojsonCheckerProcessor()
    # workflow = processor.build_workflow(argv[1])
    # print(get(workflow, 'output'))
    print("ok")
#  do i need to implement 'workflow' ?

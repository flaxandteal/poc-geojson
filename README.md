# POC - GEO-JSON Py Scripts

This is a POC tool to assist in data manipulation of GeoJson


## Installation

We recommend `pipenv`

    pipenv shell
    python3 -m pip install -r requirements.txt


## Prep

Data Resources:
    Remember to add your data resources to the `Resources` directory.
    Then add the file names to the `files_to_run.py`

## Use

Step 1:
    Run the processor to extract field data that is required.
    This should generate files in the `GeneratedRulesJsonFiles` directory.
    Containing the field names that are needed for the data manipulation from the data engineer file.

Step 2:
    Run the data engineer file and it should generate the new json documents with the newly created fields.
    The files will show in the `GeneratedGeoJsonFiles` directory

## Testing
To run tests, enter in the terminal:
1. Ensure you are in the shell: `pipenv shell`
2. To run all tests: `pytest`
3. By adding '-v' it will both run tests and show a breakdown of the tests that have ran. `pytest -v`
    